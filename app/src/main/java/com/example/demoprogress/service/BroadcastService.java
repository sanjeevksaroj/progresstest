package com.example.demoprogress.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.example.demoprogress.Ticklistener;


public class BroadcastService  extends Service {
	private static final String TAG = "BroadcastService";
	public static final String BROADCAST_ACTION = "com.websmithing.broadcasttest.displayevent";
	private final Handler handler = new Handler();
	Intent intent;
	int counter = 0;
	private long timeCountInMilliSeconds = 1 * 600;

	int countDown = 17;

    private final IBinder mBinder = new LocalBinder();

	/**
	 * method to initialize the values for count down timer
	 */
	private void setTimerValues() {
		// assigning values after converting to milliseconds
		timeCountInMilliSeconds = countDown * 60 * 1000;
	}

	List<Ticklistener> ticklisteners = new ArrayList<>();

	CountDownTimer	countDownTimer;
	
	@Override
	public void onCreate() {
		super.onCreate();
    	intent = new Intent(BROADCAST_ACTION);	
	}
	
    @Override
    public void onStart(Intent intent, int startId) {
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000); // 1 second
		setTimerValues();
		startCountDownTimer();

    }

    private Runnable sendUpdatesToUI = new Runnable() {
    	public void run() {
    		DisplayLoggingInfo();
		//	startCountDownTimer();
    	    handler.postDelayed(this, 10000); // 10 seconds
    	}
    };    
    
    private void DisplayLoggingInfo() {
    	Log.d(TAG, "entered DisplayLoggingInfo");

    	intent.putExtra("time", new Date().toLocaleString());
    	intent.putExtra("counter", String.valueOf(++counter));

    }
	
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}


	@Override
	public void onDestroy() {
        handler.removeCallbacks(sendUpdatesToUI);
		super.onDestroy();
	}

	private void startCountDownTimer() {

			countDownTimer = new CountDownTimer(timeCountInMilliSeconds, 1000) {
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onTick(long millisUntilFinished) {

				for (int i = 0;i<ticklisteners.size();i++){
					addTickListener(ticklisteners.get(i));
					ticklisteners.get(i).onTick(millisUntilFinished);

				}
               // addTickListener();

				//textViewTime.setText(hmsTimeFormatter(millisUntilFinished));
				int count =  (int) (millisUntilFinished / 1000);

				long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);

				Log.d("minutes",""+minutes);

				Log.d("Count",""+millisUntilFinished+"count"+count);

			}

			@Override
			public void onFinish() {


			}

		}.start();
		countDownTimer.start();
	}


	public void addTickListener(Ticklistener ticklistener){
    	this.ticklisteners.add(ticklistener);
	}

	public void stopCountDownTimer() {
		countDownTimer.cancel();
		stopSelf();
	}
    // This is the object that receives interactions from clients.

    public class LocalBinder extends Binder {
      public   BroadcastService getService() {
            return BroadcastService.this;
        }
    }



}
