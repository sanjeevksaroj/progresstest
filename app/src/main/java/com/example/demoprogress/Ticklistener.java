package com.example.demoprogress;

public interface Ticklistener {

    void onTick(long millisUntilFinished);
}
