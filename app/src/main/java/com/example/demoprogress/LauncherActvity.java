package com.example.demoprogress;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demoprogress.service.BroadcastService;


public class LauncherActvity extends Activity implements Ticklistener {
    private static final String TAG = "LauncherActvity";
    private Intent intent;
    TextView txtDateTime;
    TextView txtCounter;
    boolean mBound = false;

    private BroadcastService mBoundService;

    BroadcastService.LocalBinder mbinder;

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            mbinder  = (BroadcastService.LocalBinder) iBinder;
            mBoundService = mbinder.getService();
            mBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    @Override
    public void onTick(long millisUntilFinished) {
        Toast.makeText(LauncherActvity.this,"Test"+millisUntilFinished,Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button next = (Button) findViewById(R.id.next);
         txtDateTime = (TextView) findViewById(R.id.txtDateTime);
         txtCounter = (TextView) findViewById(R.id.txtCounter);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBoundService.stopCountDownTimer();
            }
        });

    }




    @Override
    public void onResume() {
        super.onResume();
        startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStart() {
        super.onStart();
        intent = new Intent(this, BroadcastService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);


    }

    @Override
    public void onStop() {
        super.onStop();
        unbindService(serviceConnection);
        mBound = false;

    }

    private void updateUI(Intent intent) {
        String counter = intent.getStringExtra("counter");
        String time = intent.getStringExtra("time");
        Log.d(TAG, counter);
        Log.d(TAG, time);


    }
}